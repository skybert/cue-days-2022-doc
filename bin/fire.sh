#! /bin/bash

set -o xtrace

setup_dirs() {
  local _workspace_dir=/tmp/workspace
  local _escenic_dir=${_workspace_dir}/escenic
  rm -rf "${_workspace_dir}"
  mkdir -p "${_escenic_dir}"
  ln -svf ~/src/2022-cue-days/build "${_escenic_dir}"
  ln -svf ~/src/build-tools "${_escenic_dir}"
}


main() {
  setup_dirs
}

main "$@"
