<img src="unix-ready-macos.png" alt="Unix pro environment in macOS"/> 

This document describes how I turned a vanilla install of macOS into a
powerful Unix environment.

## Emacs

## Homebrew
```text
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

## Spell check

```text
$ brew install aspell
```

## Terminal

```text
$ curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
```

## Set Caps Lock as Ctrl

Or else, I'll go seriously mad.

## Unix shell environment

### Git
```text
$ brew install git
```

### ZSH

I prefer the ZSH shell over BASH, but still need an updated BASH for
shell programming. The one that comes with macOS is very old,
`3.2.57`, so installing a newer bash with `brew`:

```text
$ brew install zsh
$ brew install zsh-autosuggestions
$ brew install zsh-completions
$ brew install bash
$ brew install fzf
```

### Coreutils
GNU coreutils are superior to the BSD tools that come with macOS:

```text
$ brew install coreutils
$ brew install gawk
$ brew install gsed
$ brew install gnu-getopt
$ brew install grep
```

Add this to your `~/.zshrc` to ensure the GNU coreutils are first in PATH:
```conf
export PATH=\
/usr/local/bin:\
/usr/local/Cellar/gnu-sed/4.9/libexec/gnubin:\
/usr/local/opt/coreutils/libexec/gnubin:\
/usr/local/opt/gawk/bin:\
/usr/local/opt/gnu-getopt/bin:\
/usr/local/opt/grep/libexec/gnubin:\
/usr/local/opt/openjdk@11/bin:\
$PATH
```

### PGP
```text
$ brew install gnupg
```

## Java
```text
$ brew install openjdk@11
```

Ensure this JDK's binaries are first in PATH, add these to `~/.zshrc`:

```text
export PATH=/usr/local/opt/openjdk@11/bin:$PATH
```

## Video meetings

https://www.microsoft.com/en-us/microsoft-teams/download-app#desktopAppDownloadregion

## Fonts
https://github.com/adobe-fonts/source-code-pro/releases/tag/2.038R-ro/1.058R-it/1.018R-VAR

Unzip it and drag and drop the files to the `Font Book`:
<img src="macos-font-install.png" alt="install good looking font on macOS" />
